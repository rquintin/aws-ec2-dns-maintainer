import boto3
import os
import pytest

class mock53:
    def __init__(self, **kwargs):
        self.recordSets = []
        if 'zoneId' in kwargs:
            self.zone_id = kwargs['zoneId']
        if 'recordSets' in kwargs:
            self.recordSets = kwargs['recordSets']

    def list_resource_record_sets(self, *args, **kwargs):
        records = self.recordSets
        if 'StartRecordType' in kwargs:
            records = list(filter(lambda rs: rs['Type'] == kwargs['StartRecordType'], records))

        return {'IsTruncated': False,
                'MaxItems': '100',
                'ResourceRecordSets': records,
                'ResponseMetadata': {}
                }

######################################################################
### getTXTRecords    
######################################################################    
def test_getTXTRecords_noRecordsExist():
    from ec2_dns_maintainer.ec2_dns_maintainer import getTXTRecords
    conn = mock53(recordSets = [])
    result = getTXTRecords(conn, "foo.test.tld.", "ABCD")
    assert len(result) == 0

def test_getTXTRecords_noTXTRecordsExist():
    from ec2_dns_maintainer.ec2_dns_maintainer import getTXTRecords
    conn = mock53(recordSets = [
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{'Value': '127.0.0.1'}],
         'TTL': 300,
         'Type': 'A'},
        {'Name': 'bar.test.tld.',
         'ResourceRecords': [{'Value': '127.0.0.2'}],
         'TTL': 300,
         'Type': 'A'}
        ])
    result = getTXTRecords(conn, "foo.test.tld.", "ABCD")
    assert len(result) == 0
    
def test_getTXTRecords_OneTXT():
    from ec2_dns_maintainer.ec2_dns_maintainer import getTXTRecords
    conn = mock53(recordSets = [
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{'Value': '127.0.0.1'}],
         'TTL': 300,
         'Type': 'A'},
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{"Value": "\"automation-enabled:true\""}],
         'TTL': 300,
         'Type': 'TXT'},
        {'Name': 'bar.test.tld.',
         'ResourceRecords': [{'Value': '127.0.0.2'}],
         'TTL': 300,
         'Type': 'A'}
        ])
    result = getTXTRecords(conn, "foo.test.tld.", "ABCD")
    assert len(result) == 1
    assert result == [{"Value": "\"automation-enabled:true\""}]
    
def test_getTXTRecords_nTXT():
    from ec2_dns_maintainer.ec2_dns_maintainer import getTXTRecords
    conn = mock53(recordSets = [
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{'Value': '127.0.0.1'}],
         'TTL': 300,
         'Type': 'A'},
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{"Value": "\"automation-enabled:true\""}],
         'TTL': 300,
         'Type': 'TXT'},
        {'Name': 'bar.test.tld.',
         'ResourceRecords': [{'Value': '127.0.0.2'}],
         'TTL': 300,
         'Type': 'A'},
        {'Name': 'bar.test.tld.',
         'ResourceRecords': [{"Value": "\"testing\""}],
         'TTL': 300,
         'Type': 'TXT'}
        ])
    result = getTXTRecords(conn, "foo.test.tld.", "ABCD")
    assert len(result) == 1
    assert result == [{"Value": "\"automation-enabled:true\""}]

def test_getTXTRecords_filterAutomation1():
    from ec2_dns_maintainer.ec2_dns_maintainer import getTXTRecords
    conn = mock53(recordSets = [
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{'Value': '127.0.0.1'}],
         'TTL': 300,
         'Type': 'A'},
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{"Value": "\"automation-enabled:true\""}],
         'TTL': 300,
         'Type': 'TXT'}
        ])
    result = getTXTRecords(conn, "foo.test.tld.", "ABCD", True)
    assert len(result) == 0
    
def test_getTXTRecords_filterAutomation2():
    from ec2_dns_maintainer.ec2_dns_maintainer import getTXTRecords
    conn = mock53(recordSets = [
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{'Value': '127.0.0.1'}],
         'TTL': 300,
         'Type': 'A'},
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{"Value": "\"automation-enabled:true\""}, 
             {"Value": "\"test\""}], 
         'TTL': 300,
         'Type': 'TXT'}
        ])
    result = getTXTRecords(conn, "foo.test.tld.", "ABCD", True)
    assert len(result) == 1
    assert result == [{"Value": "\"test\""}]

def test_getTXTRecords_filterAutomation3():
    from ec2_dns_maintainer.ec2_dns_maintainer import getTXTRecords
    conn = mock53(recordSets = [
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{'Value': '127.0.0.1'}],
         'TTL': 300,
         'Type': 'A'},
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{"Value": "\"automation-enabled:true\""},
             {"Value": "\"test\""}, {"Value": "\"ntest\""}],
         'TTL': 300,
         'Type': 'TXT'}
        ])
    result = getTXTRecords(conn, "foo.test.tld.", "ABCD", True)
    assert len(result) == 2
    assert result == [{"Value": "\"test\""}, {"Value": "\"ntest\""}]

######################################################################
### checkPermissions
######################################################################    
def test_checkPermissions_noARecord():
    from ec2_dns_maintainer.ec2_dns_maintainer import checkPermissions
    conn = mock53(recordSets = [
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{'Value': '127.0.0.1'}],
         'TTL': 300,
         'Type': 'A'},
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{"Value": "\"automation-enabled:true\""},
             {"Value": "\"test\""}, {"Value": "\"ntest\""}],
         'TTL': 300,
         'Type': 'TXT'}
        ])
    assert True == checkPermissions(conn, "bar.test.tld.", "ABCD")

def test_checkPermissions_noTXTRecord():
    from ec2_dns_maintainer.ec2_dns_maintainer import checkPermissions
    conn = mock53(recordSets = [
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{'Value': '127.0.0.1'}],
         'TTL': 300,
         'Type': 'A'},
        ])
    assert False == checkPermissions(conn, "foo.test.tld.", "ABCD")

def test_checkPermissions_noAuthorization1():
    from ec2_dns_maintainer.ec2_dns_maintainer import checkPermissions
    conn = mock53(recordSets = [
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{'Value': '127.0.0.1'}],
         'TTL': 300,
         'Type': 'A'},
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{"Value": "\"test\""}, {"Value": "\"ntest\""}],
         'TTL': 300,
         'Type': 'TXT'}
        ])
    assert False == checkPermissions(conn, "foo.test.tld.", "ABCD")

def test_checkPermissions_noAuthorization2():
    from ec2_dns_maintainer.ec2_dns_maintainer import checkPermissions
    conn = mock53(recordSets = [
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{'Value': '127.0.0.1'}],
         'TTL': 300,
         'Type': 'A'},
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{"Value": "\"automation-enabled:false\""},
             {"Value": "\"test\""}, {"Value": "\"ntest\""}],
         'TTL': 300,
         'Type': 'TXT'}
        ])
    assert False == checkPermissions(conn, "foo.test.tld.", "ABCD")
    
def test_checkPermissions_authorized():
    from ec2_dns_maintainer.ec2_dns_maintainer import checkPermissions
    conn = mock53(recordSets = [
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{'Value': '127.0.0.1'}],
         'TTL': 300,
         'Type': 'A'},
        {'Name': 'foo.test.tld.',
         'ResourceRecords': [{"Value": "\"automation-enabled:true\""},
             {"Value": "\"test\""}, {"Value": "\"ntest\""}],
         'TTL': 300,
         'Type': 'TXT'}
        ])
    assert True == checkPermissions(conn, "foo.test.tld.", "ABCD")

